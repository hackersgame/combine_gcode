#!/usr/bin/python3
import os


script_path = os.path.dirname(os.path.realpath(__file__))
g_code_path = f"{script_path}/g_code"

#Not 100% sure why this is needed:
g_code_betten_prints = """
M140 S0
M107

G92 E0
G92 E0
G1 F3000 E-3
;LAYER_COUNT:9
;LAYER:0
M107
;End g_code_betten_prints
"""

gcode_files = []
files_and_dirs = os.listdir(g_code_path)
for file_or_dir in files_and_dirs:
    file_or_dir = g_code_path + "/" + file_or_dir
    if os.path.isfile(file_or_dir) and file_or_dir.endswith(".gcode"):
        gcode_files.append(file_or_dir)
        #print(file_or_dir)


#find alike top
top_alike = ""
first_file_gcode  = open(gcode_files[0]).readlines()
second_file_gcode = open(gcode_files[1]).readlines()

for top_cutoff in range(0,len(first_file_gcode)):
    test_line = first_file_gcode[top_cutoff]
    if test_line.startswith(";"):
        continue
    if test_line == second_file_gcode[top_cutoff]:
        top_alike = top_alike + test_line
    else:
        break
#print(top_alike)


#find bot alike
bot_alike = ""

for bot_cutoff in range(0,len(first_file_gcode)):
    bot_cutoff = bot_cutoff * -1
    test_line = first_file_gcode[bot_cutoff]
    if test_line.startswith(";"):
        continue
    if test_line == second_file_gcode[bot_cutoff]:
        bot_alike = test_line + bot_alike
    else:
        break
#print(bot_alike)


output_data = f";Top alike in all g-files:\n" + top_alike
output_data = output_data + f"\n;End Top alike\n"
first_print = True
for gcode_path in gcode_files:
    with open(gcode_path) as fh:
        raw_gcode = fh.readlines()[top_cutoff:bot_cutoff+1]
        text_gcode = "".join(raw_gcode)
        text_gcode = f";from {gcode_path}\n" + text_gcode
        
        if not first_print:
            text_gcode = f";from g_code_betten_prints var\n" + g_code_betten_prints + text_gcode
        
        output_data = output_data + text_gcode
        first_print = False

output_data = output_data + f"\n;Bottom alike in all g-files:\n" + bot_alike
output_data = output_data + f"\n;End bottom alike\n"

print(output_data)
